var express = require('express');
var app = express();
var bodyParser = require('body-parser');
app.use(express.static('pub'));
app.use(bodyParser.json());
var logInPage = require('./route/login.js');

var USER_CODE = "NA";
module.exports.USER_CODE = USER_CODE;

var getTime = function(req,res,next){
  req.getTime = new Date().getHours() + ":" + new Date().getMinutes();
  next();
}
app.use(getTime);

var logRequest = function(req,res,next){
  console.log("Request logged at : " + req.getTime);
  next();
}
app.use(logRequest);

app.all('/',function(req,res){
  res.send("You are on home page.");
});
app.use('/login',logInPage);

app.all(/./, function (req, res, next) {
  res.send("haha! wrong url :)");
});
app.listen(3001,function(){
  console.log("hell yeah! server started on port : " + 3001);
});