var express = require('express');
var appCheck = require('../app.js');
var validate = require('./validate.js');

var router = express.Router();
router.use(function(req,res,next){
	
    var detailsObj = req.body;
    if(!validate.isValidEmail(detailsObj.email)){
        res.send("Not a valid email.");
        return;
    }
    if(validate.isBlank(detailsObj.comment)){
        res.send("comment field empty");
        return;
    }
    next();

});

router.post('/', function(req,res) {
    
    var detailsObj = req.body;
    var pg = require('pg');
    var dbConnection = 'postgres://postgres:cronj@localhost/emp';
    var client = new pg.Client(dbConnection);
    client.connect(); 
	var query = client.query("select email from employee where email = '"+detailsObj.email+"'");
    query.on('row',function(row,result){
        result.addRow(row);
    });
    query.on('error',function(error){
        console.log(error);
        client.end();
    });
    query.on('end',function(result){
        var comment = result.rows[0].email;
        if(comment==detailsObj.email){
            makeComment(detailsObj.email,res,client,detailsObj.comment);
        }else{
            res.send("User does not exist.");
            client.end();
        }
    });

});

function makeComment(email,res,client,comment){
    
    var date = (new Date().getMonth()+1)+"/"+new Date().getDate()+"/"+new Date().getFullYear();
    var query=client.query("insert into comment values('"+email+"','"+comment+"','"+date+"')");
    query.on('error',function(error){
        res.send("comment not added..");
        client.end();
    });
    query.on('end',function(result){
        res.send("comment added...");
        client.end();
    }); 

}

module.exports = router;