var express = require('express');
var appCheck = require('../app.js');
var validate = require('./validate.js');

var router = express.Router();
router.use(function(req,res,next){
	
    var detailsObj = req.body; 
    if(validate.isBlank(detailsObj.comment || validate.isBlank(detailsObj.new))){
        res.send("some field empty");
        return;
    }
    next();

});

router.post('/', function(req,res) {
    
    var detailsObj = req.body; 
	var pg = require('pg');
    var dbConnection = 'postgres://postgres:cronj@localhost/emp';
    var client = new pg.Client(dbConnection);
    client.connect();

    var query = client.query("select password from employee where email = '"+appCheck.USER_CODE+"'");
    query.on('row',function(row,result){
        result.addRow(row);
    });
    query.on('error',function(error){
        console.log(error);
        client.end();
    });
    query.on('end',function(result){
        var pass = result.rows[0].password;
        if(pass==detailsObj.old){
            changePass(detailsObj.new,res,client);
        }else{
            res.send("password not changed..");
            client.end();
        }
    });

});

function changePass(password,res,client){
    
    var query=client.query("update employee set password = '"+password+"' where email = '"+appCheck.USER_CODE+"'");
    query.on('error',function(error){
        res.send("password not changed..");
        client.end();
    });
    query.on('end',function(result){
        res.send("password changed...");
        client.end();
    }); 

}

module.exports = router;