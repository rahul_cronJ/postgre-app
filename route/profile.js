var express = require('express');
var appCheck = require('../app.js');
var router = express.Router();

router.get('/', function(req,res) {
	
    var pg = require('pg');
    var dbConnection = 'postgres://postgres:cronj@localhost/emp';
    var client = new pg.Client(dbConnection);
    client.connect();

    if(appCheck.USER_CODE!='NA' && appCheck.USER_CODE!='hr@cronj.com'){
        getProfile(appCheck.USER_CODE,client);
        getComments(appCheck.USER_CODE,client,res);
    }else{
        res.send("User not logged in");
        client.end();
    } 

});

function getProfile(email,client){
    
    var query = client.query("select email,name,mobile from employee where email = '"+email+"'");
    
    query.on('row',function(row,result){
        result.addRow(row);
    });

    query.on('error',function(error){
        console.log(error);
        client.end();
    });
    
    query.on('end',function(result){
        var user = result.rows[0];
        console.log("name: "+user.name);
        console.log("email: "+user.email);
        console.log("mobile: "+user.mobile);
    });

}

function getComments(email,client,res){
   
    var query = client.query("select comment from comment where email = '"+email+"'");
    query.on('row',function(row,result){
        result.addRow(row);
    });
    query.on('error',function(error){
        console.log(error);
        client.end();
    });
    query.on('end',function(result){
        var user = result.rows;
        console.log("total "+user.length+" comments");
        for(var i=0;i<user.length;i++)
            console.log(i+" : "+user[i].comment);
        console.log("end");
        res.send("Success, see console");
        client.end();
    });

}

module.exports = router;