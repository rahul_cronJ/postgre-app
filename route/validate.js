const EMAIL_REGEX = /^[A-Z0-9._-]+@[A-Z0-9.]+\.[A-Z]{2,4}/im;
const MOBILE_REGEX = /^[1-9]{1}[0-9]{9}/;
const NAME_REGEX = /[^a-z]/i;

function isValidEmail(email) {
	
	var regExEmail = EMAIL_REGEX;
	if(regExEmail.test(email))
		return true;
	return false;

}

function isValidMobile(mobile){
	
	var regExMobile = MOBILE_REGEX;
	if(mobile.length == 10 && regExMobile.test(regExMobile))
		return true;
	return false;

}

function isValidName(name){

	var regExName = NAME_REGEX;
	if(!regExName.test(name))
		return true;
	return false;

}

function isBlank(input){

	if(input)
		return false;
	return true;

}

module.exports.isValidEmail = isValidEmail;
module.exports.isValidMobile = isValidMobile;
module.exports.isValidName = isValidName;
module.exports.isBlank = isBlank;