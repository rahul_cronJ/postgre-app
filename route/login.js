var express = require('express');
var appCheck = require('../app.js');
var addUser = require('./adduser.js');
var changePass = require('./changepass.js');
var makeComment = require('./comment.js');
var profile = require('./profile.js');
var validate = require('./validate.js');

var router = express.Router();
router.use(function(req,res,next){
	var loginCreds = req.body;
	
	if(!validate.isValidEmail(loginCreds.email)){
		res.send("A:Not a valid email.");
		return;
	}
	
	if(validate.isBlank(loginCreds.password)){
		res.send("password field empty");
		return;
	}

	next();
});
	
router.post('/', function(req,res) {
	var loginCreds = req.body;
	var pg = require('pg');
	var dbConnection = 'postgres://postgres:cronj@localhost/emp';
	var client = new pg.Client(dbConnection);
	client.connect();
 
	var query = client.query("select * from employee");
	
	if(loginCreds.email=='hr@cronj.com' && loginCreds.password == 'hrcronj'){
		res.send("HR logged in");
    	appCheck.USER_CODE = "hr@cronj.com";
    	client.end();
    	return;
	}
	query.on("row", function (row, result) {
    	result.addRow(row);
	});
	query.on("end", function (result) {
    	var userFound = false;
    	for(var user=0;user<result.rows.length;user++){
    		if(result.rows[user].email == loginCreds.email && result.rows[user].password == loginCreds.password){
    			res.send("User logged in");
    			userFound = true;
	 		   	appCheck.USER_CODE = loginCreds.email;
    			break;
    		}
    	}
    	if(!userFound){
    		res.send("Wrong email and password");
    	}
    	client.end();
	});
});
router.use('/addUser',addUser);
router.use('/changePass',changePass);
router.use('/makeComment',makeComment);
router.use('/seeProfile',profile);
module.exports = router;