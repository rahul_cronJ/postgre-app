var express = require('express');
var appCheck = require('../app.js');
var validate = require('./validate.js');

var router = express.Router();
router.use(function(req,res,next){
    
    var detailsObj = req.body; 
    
    if(!validate.isValidEmail(detailsObj.email)){
        res.send("Not a valid email.");
        return;
    }
    if(!validate.isValidMobile(detailsObj.mobile)){
        res.send("Not a valid mobile.");
        return;
    }
    if(!validate.isValidName(detailsObj.name)){
        res.send("Not a valid name.");
        return;
    }
    if(validate.isBlank(detailsObj.password)){
        res.send("password field empty");
        return;
    }

	next();

});

	
router.post('/', function(req,res) {
    
    var detailsObj = req.body;
    var pg = require('pg');
    var dbConnection = 'postgres://postgres:cronj@localhost/emp';
    var client = new pg.Client(dbConnection);
    client.connect();
	var query = client.query("insert into employee values("
        +"'"+detailsObj.email+"'"+
        ","
        +"'"+detailsObj.password+"'"+
        ","
        +"'"+detailsObj.name+"'"+
        ","
        +"'"+detailsObj.mobile+"'"+
        ")");
    query.on('error',function(error){
        console.log(error);
    });
    
    query.on('end',function(){
        client.end();
        console.log("user added .");
        res.send("user added.");
        
    });

});

module.exports = router;